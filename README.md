# ~/ABRÜPT/SIMONE WEIL/ALLONS-NOUS VERS LA RÉVOLUTION PROLÉTARIENNE/*

La [page de ce livre](https://abrupt.ch/simone-weil/allons-nous-vers-la-revolution-proletarienne/) sur le réseau.

## Sur le livre

1933, l'histoire bascule, Simone Weil interroge : « Allons-nous vers la révolution prolétarienne ? » Sans renoncer au combat, la philosophe déploie dans ce texte crépusculaire un pessimisme critique qui augure les ténèbres à venir. Simone Weil dissèque l'impossibilité d'émancipation face à des régimes bureaucratiques qui oppressent les masses, face à l'ouvrier broyé par la machine, cantonné à un rôle de soumission à la société. Au cœur des remous de l'époque, elle met dos à dos l'URSS de Staline et le fascisme naissant du Troisième Reich, refuse les positions trotskistes, et laisse entendre, malgré une certaine fatalité, une voie possible au travers de l'idée anarchiste de souveraineté des travailleurs, et non de celle du travail au mépris des travailleurs. Par cette organisation horizontale du travail, elle écarte l'idée de défaite, et recherche par tous les moyens possibles la lutte au nom de tout ce qui fait « la valeur de la vie humaine ».

## Sur l'autrice

1909, naissance, 1943, mort. Une vie brève mais qui se jeta intensément dans la question sociale du siècle. Simone Weil fait partie de ces rares penseurs qui accordent leur existence à leur pensée. Anarcho-syndicaliste et critique éclairée de Marx, christique et non chrétienne, de tous les combats pour défendre les sans-voix, malgré une érudition rare qui la mena de la mathématique au sanskrit, et qui aurait pu lui offrir la vie paisible du professorat, cette « martienne », selon le mot d’Alain, avait la merveilleuse intransigeance de la cohérence. Elle se rendit comme ouvrière à l’usine de 1934 à 1935, en dépit de sa santé fragile, puis en 1936, elle partit en Espagne pour soutenir ses camarades républicains, ne put accepter leurs écarts, continua à lutter, à écrire, développa une mystique de la justice et de la souffrance de l’absence de justice. Durant la guerre, elle se rendit à Londres pour rejoindre la France libre, ne pouvant supporter le confort américain. Elle voulut rentrer en France en tant que résistante, on le lui refusa. Tuberculeuse, à l’orée de la mort, par solidarité avec ses compatriotes subissant les restrictions de l’occupation, elle ne s’alimenta quasiment plus, et jusqu’à son dernier souffle, Simone Weil demeura fidèle à ce qu’elle défendait.

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
